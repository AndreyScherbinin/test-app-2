package org.spez.testapp2.app.tape;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

public class TapePlaylist {

    private int count;
    public String[] paths;

    //Create playlist on create
    //Update playlist on popular content changes
    //Update playlist on favourite content changes
    //Update playlist on new content appearing
    //Update playlist on new content hiding
    //Listen to dataset changes
    //Return count of items in playlist
    //Return uri (path) of an item with given position

    public void GeneratePlaylist(Context cont) {
        ContentResolver contentResolver = cont.getContentResolver();
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] mProjection =
                {
                        MediaStore.Images.Media.DATA
                };
        String mSortOrder = "TITLE";
        Cursor cursor = contentResolver.query(uri, mProjection, null, null, mSortOrder);
        if (cursor == null) {
            // query failed, handle error.
        } else if (!cursor.moveToFirst()) {
            // no media on the device
        } else {
            count = cursor.getCount();
            paths = new String[count];
            cursor.moveToFirst();
            int dataColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            for (int i = 0; i < count; i++) {
                paths[i] = cursor.getString(dataColumn);
                cursor.moveToNext();
            }
        }
    }

    public int getCount() {
        return count;
    }
}
