package org.spez.testapp2.app.tape;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import org.spez.testapp2.app.ui.RecyclingImageView;
import org.spez.testapp2.app.util.ImageCache;
import org.spez.testapp2.app.util.ImageResizer;

public class Tape {

    public TapeLayout2 tapeLayout;
    public TapePlaylist tapePlaylist;

    private int mImageWidth;
    private int mImageHeight;

    private TapeImageAdapter mAdapter;
    private ImageResizer mImageResizer;

    private static final String IMAGE_CACHE_DIR = "image_cache";

    //Create tape with desired tape layout N and playlist M
    public Tape(Context cont, FragmentManager fragmentManager, FrameLayout flForTape) {
        tapePlaylist = new TapePlaylist();

        mAdapter = new TapeImageAdapter(cont);

        tapeLayout = new TapeLayout2(cont, flForTape, mAdapter);
        int[] view_size = tapeLayout.CalculateLayout();
        if ((view_size[0] > 0) && (view_size[1] > 0)) {
            mImageWidth = view_size[0];
            mImageHeight = view_size[1];
            Toast.makeText(cont, "Width: " + mImageWidth + ", height: " + mImageHeight, Toast.LENGTH_SHORT).show();
        }

        mAdapter.setLayoutParams();

        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(cont, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        mImageResizer = new ImageResizer(cont, mImageWidth, mImageHeight);
//        mImageResizer.setLoadingImage(R.drawable.empty_photo);
        mImageResizer.addImageCache(fragmentManager, cacheParams);
    }

    //Fill tape with given layout and playlist
    public void FillTape(Context cont) {
        tapePlaylist.GeneratePlaylist(cont);

//        //A variant without adapter
//        if (tapePlaylist.paths != null) {
//            if ((tapePlaylist.paths.length == 1) && (tapePlaylist.paths[0].equals(""))) {
//                Toast.makeText(cont, "No images on device", Toast.LENGTH_SHORT).show();
//            } else {
//                tapeLayout.DeleteViews();
//                for (String path : tapePlaylist.paths) {
//                    tapeLayout.AddTapeView(cont, path);
//                }
//            }
//        }

        //A variant with adapter
        tapeLayout.setTapeAdapter();

    }

    //Add view to layout N and start background data loading for it
    private void AddView() {
    }

    //Shift tape one view ahead
    //Shift tape one view back

    public void onResume() {
        mImageResizer.setExitTasksEarly(false);
        mAdapter.notifyDataSetChanged();
    }

    public void onPause() {
        mImageResizer.setPauseWork(false);
        mImageResizer.setExitTasksEarly(true);
        mImageResizer.flushCache();
    }

    public void onDestroy() {
        mImageResizer.closeCache();
    }

    public void clearCache() {
        mImageResizer.clearCache();
    }

    public class TapeImageAdapter extends BaseAdapter {

        private final Context mContext;
//        private int mItemHeight = 0;
        private int mNumColumns = 0;
//        private int mActionBarHeight = 0;
        private GridView.LayoutParams mImageViewLayoutParams;

        public TapeImageAdapter(Context context) {
            super();
            mContext = context;
//            // Calculate ActionBar height
//            TypedValue tv = new TypedValue();
//            if (context.getTheme().resolveAttribute(
//                    android.R.attr.actionBarSize, tv, true)) {
//                mActionBarHeight = TypedValue.complexToDimensionPixelSize(
//                        tv.data, context.getResources().getDisplayMetrics());
//            }
        }

        private void setLayoutParams() {
//            mImageViewLayoutParams = new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mImageViewLayoutParams = new GridView.LayoutParams(mImageWidth, mImageHeight);
        }

        @Override
        public int getCount() {
            return tapePlaylist.getCount();
        }

        @Override
        public Object getItem(int position) {
            return tapePlaylist.paths[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            //One type of views - the normal ImageView
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {
            ImageView imageView;
            if (convertView == null) { // if it's not recycled, instantiate and initialize
                imageView = new RecyclingImageView(mContext);
//                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(mImageViewLayoutParams);
            } else { // Otherwise re-use the converted view
                imageView = (ImageView) convertView;
            }

//            // Check the height matches our calculated column width
//            if (imageView.getLayoutParams().height != mItemHeight) {
//                imageView.setLayoutParams(mImageViewLayoutParams);
//            }

            // Finally load the image asynchronously into the ImageView, this also takes care of
            // setting a placeholder image while the background thread runs
            mImageResizer.loadImage(tapePlaylist.paths[position], imageView);
            return imageView;
        }

//        /**
//         * Sets the item height. Useful for when we know the column width so the height can be set
//         * to match.
//         *
//         * @param height
//         */
//        public void setItemHeight(int height) {
//            if (height == mItemHeight) {
//                return;
//            }
//            mItemHeight = height;
//            mImageViewLayoutParams =
//                    new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mItemHeight);
//            mImageResizer.setImageSize(height);
//            notifyDataSetChanged();
//        }

        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }

        public int getNumColumns() {
            return mNumColumns;
        }
    }

}
