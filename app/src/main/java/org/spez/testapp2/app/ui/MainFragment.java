package org.spez.testapp2.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.spez.testapp2.app.R;
import org.spez.testapp2.app.tape.Tape;


public class MainFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "MainFragment";

    Button btnCreate;
    Button btnFill;
    Button btnClear;

    Tape tape;
    FrameLayout flForTape;

    /**
     * Empty constructor as per the Fragment documentation
     */
    public MainFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.activity_main, container, false);

        flForTape = (FrameLayout) v.findViewById(R.id.flForTape);
        btnCreate = (Button) v.findViewById(R.id.btn_create);
        btnFill = (Button) v.findViewById(R.id.btn_fill);
        btnClear = (Button) v.findViewById(R.id.btn_clear);
        btnCreate.setOnClickListener(this);
        btnFill.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tape != null) {
            tape.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tape != null) {
            tape.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tape != null) {
            tape.onDestroy();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create:
                if (tape == null) {
                    tape = new Tape(getActivity(), getActivity().getSupportFragmentManager(), flForTape);
                } else {
                    Toast.makeText(getActivity(), R.string.tape_exists_toast, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_fill:
                if (tape != null) {
                    tape.FillTape(getActivity());
                } else {
                    Toast.makeText(getActivity(), R.string.tape_null_toast, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_clear:
                if (tape != null) {
                    tape.clearCache();
                    Toast.makeText(getActivity(), R.string.clear_cache_complete_toast, Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    Toast.makeText(getActivity(), R.string.tape_null_toast, Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
