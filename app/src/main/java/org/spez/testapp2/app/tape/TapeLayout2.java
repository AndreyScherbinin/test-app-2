package org.spez.testapp2.app.tape;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Toast;

public class TapeLayout2 {

    //It is a default animated layout for NewCapp with a sinusoid-distributed views
    //| -|- --|-- ----|---- --|-- -|- |
    //| --- ----- --------- ----- --- |

    private Context cont;
    private FrameLayout flForTape;
    private TapeAdapterView containerV;
    private Tape.TapeImageAdapter mAdapter;

    //Constants for layout
    private static int L_VIEWS_NUMBER = 5; //Number of views displayed on the screen
    private static final int L_VIEWS_PADDING = 10; //Padding between views in pixels
    private static final int L_UPPER_VIEW_WEIGHT = 4; //Weight of the upper view
    private static final int L_BOTTOM_VIEW_WEIGHT = 1; //Weight og the bottom view - mirrored and flipped
    private static final double L_CENTER_VIEW_SCALE = 1.2; //Scaling factor of center group view

    //Constants for animation
    private static final int ANIM_DEFAULT_DURATION = 1000; //Default duration of animation in msecs
    private static final boolean ANIM_LOOP_VIEWS = true; //Defines whether views should loop

    private static int numVisibleViews; //Number of active views
    private static int activeOffset; //Position of active view in the window (start from 0)
    private static TapeViewProperties[] tvps;

    public TapeLayout2(Context context, FrameLayout frameLayoutForTape, Tape.TapeImageAdapter adapter) {
        cont = context;
        flForTape = frameLayoutForTape;
        mAdapter = adapter;
        //Check whether L_VIEWS_NUMBER is odd
        if ((L_VIEWS_NUMBER & 1L) != 1) {
            L_VIEWS_NUMBER++;
            Toast.makeText(cont, "Set views number to odd!", Toast.LENGTH_SHORT).show();
        }
        numVisibleViews = L_VIEWS_NUMBER + 2; //We add one invisible view from each side (start and end)
        activeOffset = numVisibleViews / 2; //Active view is in the middle of the window

        containerV = new TapeAdapterView(cont, numVisibleViews, activeOffset);
        containerV.setBackgroundColor(Color.BLACK);
        flForTape.addView(containerV, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    //Calculate numeric values of properties of tape views based on the required number of views
    public int[] CalculateLayout() {
        int[] res = new int[2];

        int fl_width = flForTape.getWidth();
        int fl_height = flForTape.getHeight();

        tvps = new TapeViewProperties[numVisibleViews];
        double sumWidth = 0.0;
        float degreeStep = 180.0f / ((float) (numVisibleViews - 1));
        for (int i = 0; i < numVisibleViews; i++) {
            TapeViewProperties tvp = new TapeViewProperties();
            tvps[i] = tvp;
            tvps[i].rotationY = -90.0f + ((float) i) * degreeStep;
            tvps[i].alpha = 1.0f;
            tvps[i].lViewWidthCoef = Interpolate(numVisibleViews - 1, numVisibleViews - 1 - i);
            sumWidth += tvps[i].lViewWidthCoef;
            tvps[i].lViewHeight = fl_height;
        }
        double xWidth = ((double) (fl_width - L_VIEWS_PADDING * (L_VIEWS_NUMBER - 1))) / sumWidth;
        int widthPos = fl_width;
        for (int i = 0; i < numVisibleViews; i++) {
            tvps[i].lViewWidth = (int) Math.round((xWidth) * tvps[i].lViewWidthCoef);
            widthPos -= tvps[i].lViewWidth;
            if ((i > 0) && (i < numVisibleViews - 2)) {
                widthPos -= L_VIEWS_PADDING;
            }
            tvps[i].x = widthPos;
        }

        res[0] = tvps[activeOffset].lViewWidth;
        res[1] = tvps[activeOffset].lViewHeight;

        return res;
    }

    private double Interpolate(int count, int pos) {
        return (1 - Math.abs(Math.cos(((double) pos) / ((double) count) * Math.PI)));
    }

    public void setTapeAdapter() {
        containerV.setAdapter(mAdapter);
    }

    private class TapeViewProperties {
        public int x; //left position
        public float rotationY;
        public float alpha;
        public double lViewWidthCoef;
        public int lViewWidth;
        public int lViewHeight;

        TapeViewProperties() {
            this.x = 0;
            this.rotationY = 0.0f;
            this.alpha = 0.0f;
            this.lViewWidthCoef = 0.0;
            this.lViewWidth = 0;
            this.lViewHeight = 0;
        }

    }

    private class TapeAdapterView extends AdapterView<Tape.TapeImageAdapter> {

        //An adapter associated with this view group
        private Tape.TapeImageAdapter mAdapter;

        //Maximum number of views in layout
        private int mMaxNumActiveViews;

        //Offset of the active view within the displayed views
        //Starts from 0
        private int mActiveOffset;

        //The index, relative to the adapter, of the beginning of the window of views
        private int mCurrentWindowStart = 0;

        //The index, relative to the adapter, of the end of the window of views
        private int mCurrentWindowEnd = -1;

        //Stores computed child dimensions while performing on layout
        private final Rect mTmpContainerRect = new Rect();
        private final Rect mTmpChildRect = new Rect();

//        int mWhichChild = 0;
//        boolean mLoopViews = ANIM_LOOP_VIEWS;
//
//        class ViewAndMetaData {
//            View view;
//            int relativeIndex;
//            int adapterPosition;
//            long itemId;
//            ViewAndMetaData(View view, int relativeIndex, int adapterPosition, long itemId) {
//                this.view = view;
//                this.relativeIndex = relativeIndex;
//                this.adapterPosition = adapterPosition;
//                this.itemId = itemId;
//            }
//        }

        private TapeAdapterView(Context cont, int maxNumActiveViews, int activeOffset) {
            super(cont);

            if (activeOffset > maxNumActiveViews - 1) {
                //TODO: throw an exception here
            }
            else {
                mMaxNumActiveViews = maxNumActiveViews;
                mActiveOffset = activeOffset;
            }
        }

        @Override
        public void setAdapter(Tape.TapeImageAdapter adapter) {
            Toast.makeText(cont, "setAdapter", Toast.LENGTH_SHORT).show();

            mAdapter = adapter;
            setFocusable(false);

            mCurrentWindowStart = 0;
            mCurrentWindowEnd = mCurrentWindowStart + mMaxNumActiveViews;

            requestLayout();
        }

        @Override
        public Tape.TapeImageAdapter getAdapter() {
            return mAdapter;
        }

        @Override
        public void setSelection(int position) {
            //TODO: write the code
        }

        @Override
        public View getSelectedView() {
            //TODO: it is stub return value; write the code
            return null;
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(flForTape.getWidth(), flForTape.getHeight());
        }

        @Override
        protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
            Toast.makeText(cont, "onLayout", Toast.LENGTH_SHORT).show();

            final int count = getChildCount();

            for (int i = 0; i < count; i++) {
                final View child = getChildAt(i);

                //Index from tvps[] view params of which should be assigned to child
                int corrIndex;

                if (i < mCurrentWindowStart) {
                    child.setVisibility(GONE);
//                    corrIndex = 0; //the first
                } else if (i > mCurrentWindowEnd) {
                    child.setVisibility(GONE);
//                    corrIndex = tvps.length - 1; //the last
                } else {
                    final LayoutParams lp = (LayoutParams) child.getLayoutParams();
                    child.setVisibility(VISIBLE);
                    corrIndex = Math.min(Math.max(i - mCurrentWindowStart, 0), tvps.length - 1); //ensure we are not out of index

                    child.setRotationY(tvps[corrIndex].rotationY);
                    child.setAlpha(tvps[corrIndex].alpha);

                    final int width = child.getMeasuredWidth();
                    final int height = child.getMeasuredHeight();

                    mTmpContainerRect.top = top;
                    mTmpContainerRect.bottom = bottom;
                    mTmpContainerRect.left = tvps[corrIndex].x;
                    mTmpContainerRect.right = tvps[corrIndex].x + tvps[corrIndex].lViewWidth;

                    Gravity.apply(Gravity.CENTER, width, height, mTmpContainerRect, mTmpChildRect);

                    // Place the child.
                    child.layout(mTmpChildRect.left, top, mTmpChildRect.right, bottom);
                }
            }
        }

        void transformViewForTransition(int fromIndex, int toIndex, View view, boolean animate) {

//            if (fromIndex == -1 && toIndex == 0) { //View enters the window from start
//
//            } else if (fromIndex == -1 && toIndex == mMaxNumActiveViews - 1) { //View enters the window from end
//
//            } else { //View moves within the window
//
//            }

//            if (animate) {
//                PropertyValuesHolder x = PropertyValuesHolder.ofInt("x", tvps[toIndex].x);
//                PropertyValuesHolder rotationY = PropertyValuesHolder.ofFloat("rotationY", tvps[toIndex].rotationY);
//                PropertyValuesHolder alpha = PropertyValuesHolder.ofFloat("alpha", tvps[toIndex].alpha);
//                ObjectAnimator oa = ObjectAnimator.ofPropertyValuesHolder(view, x, rotationY, alpha);
//                oa.setDuration(ANIM_DEFAULT_DURATION);
////                oa.setInterpolator(new AccelerateInterpolator(1.0f));
//                oa.start();
//            } else {
//                view.setX(tvps[toIndex].x);
//                view.setRotationY(tvps[toIndex].rotationY);
//                view.setAlpha(tvps[toIndex].alpha);
//            }

            Toast.makeText(cont, "" + toIndex + "  " + tvps[toIndex].x + "  " +
                    tvps[toIndex].rotationY + "  " + tvps[toIndex].alpha, Toast.LENGTH_SHORT).show();
            view.setX(300f);
            view.setRotationY(45f);
            view.setAlpha(1.0f);
        }

//        @Override
//        public void showNext() {
//
//        }
//
//        @Override
//        public void showPrevious() {
//
//        }
//
//        @Override
//        public void advance() {
//
//        }

    }

}
