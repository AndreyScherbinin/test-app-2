package org.spez.testapp2.app.tape;

import android.content.Context;
import android.graphics.Color;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class TapeLayout1 {

    //It is a horizontal scroll view with a linear layout as a content holder

    private FrameLayout flForTape;
    private LinearLayout containerV;
    private Tape.TapeImageAdapter mAdapter;

    //Constants for layout
    final int L_VIEWS_NUMBER = 5;
    final int L_VIEWS_SPACE = 0;
    final int L_UP_VIEW_WEIGHT = 4;
    final int L_BOTTOM_VIEW_WEIGHT = 1;
    int L_VIEW_WIDTH;
    int L_VIEW_HEIGHT;

    //Constants for animation
    final int ANIM_STILL_TIME = 5000;
    final int ANIM_TRANSITION_TIME = 5000;

    //Arrays for layout views parameters
    private float positionX[], positionY[];
    private float rotationX[], rotationY[];
    private float alpha[];
    private float scale[];

    //Array for
    private int pos[];

    //Create layout inside given frame layout
    public TapeLayout1(final Context cont, FrameLayout frameLayoutForTape, Tape.TapeImageAdapter adapter) {

        flForTape = frameLayoutForTape;
        mAdapter = adapter;

//        //Horizontal scroll view - a variant without adapter
        HorizontalScrollView hsv = new HorizontalScrollView(cont);
        hsv.setBackgroundColor(Color.BLACK);
        flForTape.addView(hsv, HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.MATCH_PARENT);
        containerV = new LinearLayout(cont);
        containerV.setOrientation(LinearLayout.HORIZONTAL);
        hsv.addView(containerV, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);

        //GridView - a variant with adapter
//        containerV = new GridView(cont);
//        containerV.setBackgroundColor(Color.BLACK);
//        flForTape.addView(containerV, GridView.LayoutParams.WRAP_CONTENT, GridView.LayoutParams.MATCH_PARENT);

    }

    //Calculate numeric values of properties of tape views based on the required number of views
    public int[] CalculateLayout() {
        int[] res = new int[2];

        int fl_width = flForTape.getWidth();
        int fl_height = flForTape.getHeight();

        L_VIEW_WIDTH = (fl_width - L_VIEWS_SPACE * (L_VIEWS_NUMBER - 1)) / L_VIEWS_NUMBER;
//        L_VIEW_HEIGHT = fl_height * L_UP_VIEW_WEIGHT / (L_UP_VIEW_WEIGHT + L_BOTTOM_VIEW_WEIGHT);
        L_VIEW_HEIGHT = fl_height;

        res[0] = L_VIEW_WIDTH;
        res[1] = L_VIEW_HEIGHT;

        return res;
    }

//    public void setTapeAdapter() {
//        containerV.setNumColumns(L_VIEWS_NUMBER);
//        containerV.setStretchMode(GridView.NO_STRETCH);
//        containerV.setColumnWidth(L_VIEW_WIDTH);
//        containerV.setAdapter(mAdapter);
//    }

    public void AddTapeView(Context cont, String path) {
//        LinearLayout llChild = new LinearLayout(cont);
//        llChild.setOrientation(LinearLayout.VERTICAL);
//        containerV.addView(llChild, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        LinearLayout.LayoutParams ivpUp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0);
//        ivpUp.gravity = Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL;
//        ivpUp.weight = L_UP_VIEW_WEIGHT;
//        LinearLayout.LayoutParams ivpBottom = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0);
//        ivpBottom.gravity = Gravity.TOP|Gravity.CENTER_HORIZONTAL;
//        ivpBottom.weight = L_BOTTOM_VIEW_WEIGHT;

        ImageView[] iv = new ImageView[2];
        iv[0] = new ImageView(cont);
        containerV.addView(iv[0], L_VIEW_WIDTH, L_VIEW_HEIGHT);
        iv[0] = (ImageView) mAdapter.getView(containerV.indexOfChild(iv[0]), iv[0], containerV);

//        iv[1] = new ImageView(cont);
//        iv[1].setRotationX(-180);
//        iv[1].setAlpha(0.25f);
//        llChild.addView(iv[1], ivpBottom);

//        TapeDataLoaderTask task = new TapeDataLoaderTask(iv[0]);
//        task.setDimensions(L_VIEW_WIDTH, L_VIEW_HEIGHT);
//        task.execute(path);
//        TapeDataLoaderTask task2 = new TapeDataLoaderTask(iv[1]);
//        task2.execute(path);
    }

    public void DeleteViews() {
        containerV.removeAllViews();
    }

    public boolean DeleteView(int pos) {
        return true;
    }

    public boolean MoveView(int curPos, int targetPos) {
        return true;
    }

    private void ApplyViewProperties(int pos) {
    }

    public void ShiftTapeAhead() {
    }

    public void ShiftTapeBack() {
    }

}
